#!/bin/env bash

for dir in binpkgs distfiles
do 
  if [ ! -d ${dir} ]; then
  mkdir -p ${PWD}/cache/${dir}
  fi
done

podman build -v /var/db/repos/gentoo:/var/db/repos/gentoo:ro -v ${PWD}/cache/binpkgs:/var/cache/binpkgs:rw -v ${PWD}/cache/distfiles:/var/cache/distfiles:rw -t localhost/gentoo-qt-builder:latest .
