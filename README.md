# Build Qt in gentoo containers.

## Usage

### Prerequisite

- Gentoo x86_64 host system (other distro is out of scope)
- Gentoo portage tree directory is located on /var/db/repos/gentoo
- [podman](https://podman.io) is installed (docker is not tested but it may work by replacing "podman" with "docker" in scripts)

### Build gentoo-qt-builder container image.

You should run all scripts as *non-root* user (rootless podman)

```Shell
$ bash buildcontainer.sh
```

If container image build succeed, you can see it as following.

```Shell
$ podman images
REPOSITORY                   TAG               IMAGE ID      CREATED         SIZE
localhost/gentoo-qt-builder  latest            170ad6604531  23 seconds ago  1.55 GB
docker.io/gentoo/stage3      systemd-20221003  930975a1e499  3 days ago      1.03 GB
```

### Edit `buildenv` file to adjust parameters

```
QT_GCC_PROFILE=x86_64-pc-linux-gnu-12.2.0
QT_RELEASE=v5.15.6-lts-lgpl
QT_REPO_URL=https://code.qt.io/qt/qt5.git
```
If you want to change gcc version to 11 for building Qt, set `QT_GCC_PROFILE` value like `x86_64-pc-linux-gnu-11.3.0`.

If you want to change Qt version to build, set `QT_RELEASE` value to another branch/tag name.

At present building Qt6 is not supported.

### Build Qt

Run `buildqt.sh` script with specifiying your envfile

```Shell
$ bash buildqt.sh ./buildenv
```
When building Qt is completed, you can find source, build, and Qt binaries under the `./qt` directory.

If any envfile isn't specified, the script check and list gcc profiles in the container.

### Customization

If you want to customize qt modules list to build, edit `files/sets/qt-subsets` and `files/build.sh` as you like.

- `files/sets/qt-subsets` is the [sets](https://wiki.gentoo.org/wiki/Package_sets) file of gentoo portage to install dependencies for building Qt.
- `files/build.sh` is the script to build Qt in a container.

If you want to change default gcc version([Gentoo slot](https://devmanual.gentoo.org/general-concepts/slotting/index.html)) to install and use, edit `Containerfile` to set parameters.

```Dockerfile
# set gcc profile for building qt
ARG DEFAULT_GCC_PROFILE=x86_64-pc-linux-gnu-12.2.0
# Set gcc slot of gentoo
ARG GCC_SLOT=12
```

The `./cache` directory is used for distfiles and binpkgs. If you don't want to use them, edit scripts as you like.
