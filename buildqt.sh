#!/bin/env bash

BUILDCONTAINER_IMAGE=localhost/gentoo-qt-builder
TAG=latest

if [ ! -d ${PWD}/qt ]; then
  mkdir -p ${PWD}/qt
fi

if [ x$1 == "x" ]; then
  echo "You must specify a envfile to build Qt with appropriate options like..."
  echo ""
  echo "bash ./buildqt.sh ./buildenv"
  echo ""

  echo "Checking which gcc profiles are supported in the container..."

  sleep 1
  podman run -it --rm  -v /var/db/repos/gentoo:/var/db/repos/gentoo:ro --entrypoint /bin/bash ${BUILDCONTAINER_IMAGE}:${TAG}  eselect gcc list

  exit 1

else
  ENVFILE=$1
  podman run -it --rm  -v /var/db/repos/gentoo:/var/db/repos/gentoo:ro -v ${PWD}/cache/distfiles:/var/cache/distfiles:rw -v ${PWD}/cache/binpkgs:/var/cache/binpkgs:rw -v ${PWD}/qt:/qt:rw --env-file=${ENVFILE} ${BUILDCONTAINER_IMAGE}:${TAG}
fi

exit $?
