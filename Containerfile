# set tag name gentoo base container image
ARG TAG=systemd-20221003
FROM docker.io/gentoo/stage3:$TAG as base

# set gcc profile for building qt
ARG DEFAULT_GCC_PROFILE=x86_64-pc-linux-gnu-12.2.0
# Set gcc slot of gentoo
ARG GCC_SLOT=12

COPY files/sets /etc/portage/sets
COPY files/package.accept_keywords /etc/portage/package.accept_keywords
COPY files/make.conf /etc/portage/

# build latest gcc slot
RUN emerge -vDN --autounmask y --autounmask-write --autounmask-continue --buildpkg --usepkg sys-devel/gcc:$GCC_SLOT dev-vcs/git sys-devel/mold gcc-config && eselect gcc set $DEFAULT_GCC_PROFILE

FROM base

ENV DEFAULT_GCC_PROFILE=""
ENV QT_REPO_URL=""
ENV QT_RELEASE=""

WORKDIR /
COPY files/build.sh /build.sh
# Uncomment the line below if you want to use mold and it is supported by gcc which you want to use(gcc 12 or later)
#COPY files/make.conf_mold /etc/portage/make.conf
RUN emerge -vDN --onlydeps --onlydeps-with-rdeps n --autounmask y --autounmask-write --autounmask-continue --buildpkg --usepkg @qt-subsets

ENTRYPOINT /build.sh
