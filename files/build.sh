#!/bin/env bash

QTDIR=/qt
QTSRCDIR=${QTDIR}/source
QT5DIR=${QTSRCDIR}/qt5-${QT_RELEASE}
QTBUILDDIR=${QTDIR}/build-${QT_RELEASE}-${QT_GCC_PROFILE}

for dir in ${QTSRCDIR} ${QTBUILDDIR}
do
  if [ ! -d ${dir} ]; then
    mkdir -p ${dir}
  fi
done

# Commma separated list of qt submodules
QT_SUBMODULES="qtbase,qtdeclarative,qtquickcontrols2,qtwayland"

if [ x${QT_RELEASE} == "x" ]; then
  QT_RELEASE="v5.15.6-lts-lgpl"
fi

if [ x${QT_GCC_PROFILE} != "x" ]; then
  eselect gcc set ${QT_GCC_PROFILE}
  source /etc/profile
fi

gcc --version

sleep 3

if [ ! -d ${QT5DIR} ]; then
  echo "Clone Qt Repositoy"
  cd ${QTSRCDIR}
  git clone ${QT_REPO_URL} --branch $QT_RELEASE ${QT5DIR}

  cd ${QT5DIR}
  perl init-repository --module-subset=${QT_SUBMODULES}
fi

cd ${QTBUILDDIR}

echo "Start Qt ${QT_RELEASE} build"

${QT5DIR}/configure -prefix /qt/${QT_RELEASE}-${QT_GCC_PROFILE} -confirm-license -opensource

make -j $(nproc)
make install
